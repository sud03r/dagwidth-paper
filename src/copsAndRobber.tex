Let $G=(V, E)$ be the control flow graph of a structured program $P$.
Recall that we characterize a loop element $L$ by its \emph{entry} and \emph{exit} points and refer to it by
the pair $(L^{\entry}, L^{\exit})$. 
%We will refer to the set of loop elements nested directly under $L$ by $\mathcal{L}$.
%
We now present the following strategy $f$ for the cop player in the cops and robber game on $G$ with \emph{three} 
cops. 
\begin{enumerate}
	\item We will throughout the game maintain that at this point $X(1)$ occupies $L^{\entry}$, $X(2)$ occupies $L^{\exit}$, and $r \in \inside(L)$, for
some loop element $L$.

(In the first round $L \coloneqq L_\phi$, where $L_\phi$ is the hypothetical loop element that encloses $G$.  Regardless of the initial position of the robber, $r\in \inside(L_\phi)$. The cops $X(1)$ and
$X(2)$ are not used in the initial step.)

		
	\item Now we move the cops: 
		\begin{enumerate}
			\item If $r \in \belongs(L)$, move $X(3)$ to $r$. 
			\item Else, since $r\in \inside(L)$, we must have 
				$r \in \inside(L_i)$ for some loop $L_i$
				directly nested under $L$.
				Move $X(3)$ to $L_i^{\exit}$.
		\end{enumerate}
	
	\item Now the robber moves, say to $r'$. Note that $r' \in (\inside(L)\cup\{\texttt{stop}\})$ since $r\in \inside(L)$ and
		$X(1)$ and $X(2)$ block all paths from there to $(\outside(L) \setminus \{\texttt{stop}\})$.

	\item One of four cases is possible:
		\begin{enumerate}
			\item $r' = X(3)$. Then we have now caught the robber and we are done.
			\item $r' = \texttt{stop}$. Move $X(3)$ to \texttt{stop} and we will catch the robber in next move since the robber cannot 
				leave \texttt{stop}.
			\item $r' \in \inside(L_i)$, i.e., the robber stayed inside the same loop that it was before.  Go to step 5.
			\item $r' \in (\inside(L) \setminus \inside(L_i))$, i.e., the robber left the inside of the loop that it was in. Go back to step $2$.
		\end{enumerate}

	\item We reach this case only if the robber $r'$ is inside $L_i$,
	and $X(3)$ had moved to $L_i^{\exit}$ in the step before.  Thus
	cop $X(3)$ now blocks movements of $r'$ to 
	$(\outside(L_i) \setminus \{\texttt{stop}\})$.  We must do one
	more round before being able to recurse:
	\begin{enumerate}
		\item Move $X(1)$ to $L_i^{\entry}$.
		\item The robber moves, say to $r''$.
			By the above, $r'' \in (\inside(L_i)\cup \texttt{stop})$.
		\item If $r'' = L_i^{\entry}$, we have caught
			the robber.
		If $r'' = \texttt{stop}$, we can catch the
						robber in the next move.
		\item In all remaining cases, $r'' \in \inside(L_i)$. Go back to step $1$ with $L \coloneqq L_i$, $X(2)$ as $X(3)$ and
						$X(3)$ as $X(2)$.
				\end{enumerate}
\end{enumerate}

For a step-by-step annotated example, see Appendix~\ref{appendix:example}.
It should be intuitive that we make progress if we reach Step (5), since we
have moved to a loop that is nested more deeply.  It is much less obvious
why we make progress if we reach 4(a).  To prove this, we introduce the
notion of a distance function $\dist(v,L^{\exit})$, which measures roughly 
the length of the longest path from $v$ to $L^{\exit}$, except that we do
not count vertices that are inside loops nested under $L$.  Formally:
%We now introduce the notion of a distance function which will be used in the subsequent proofs.
\begin{definition}
Let $L$ be a loop element of $G$ and $v \in \inside(L)$. Define 
$\dist(v, L^{\exit}) = \max_{P}(|P\cap\belongs(L)|),$
where $P$ is a directed simple path from $v$ to $L^{\exit}$ that uses only vertices
in $\inside(L)$ and does not use $L^{\entry}$.
\end{definition}

\begin{lemma}
\label{lem:move}
When the robber moves from $r$ to $r'$ in step (3), then
$\dist(r',L^{\exit})\leq \dist(r,L^{\exit})$.
The inequality is strict 
if $r\in \belongs(L)$ and $r'\neq r$.
\end{lemma}
\begin{proof}
Let $P$ be the directed path from $r$ to $r'$ along which the robber moves.
Notice that $L^{\entry}\not\in P$ since $X(1)$ is on $L^{\entry}$.
Let $P'$ be the path that achieves the maximum in $\dist(r',L^{\exit})$;
by definition $P'$ does not contain $L^{\entry}$.  


$P\cup P'$ may contain directed cycles, but if $C$ is such a cycle
then no vertices of $C$ are in $\belongs(L)$ by 
Corollary~\ref{cor:cycle}.
So if we let $P_s$ be what remains of $P\cup P'$ after removing all
directed cycles then $P_s\cap \belongs(L) = (P\cup P') \cap \belongs(L)$.
Since $P_s$ is a simple directed path from $r$ to $L^{\exit}$ that
does not use $L^{\entry}$, therefore
$\dist(r,L^{\exit}) \geq |P_s\cap \belongs(L)|\geq 
|P' \cap \belongs(L)| = \dist(r',L^{\exit})$ as desired.
If $r'\neq r$, then $P'$ cannot possibly
include $r$ while $P_s$ does, and so  if additionally
$r\in \belongs(L)$ then the inequality is strict.
\end{proof}

\begin{lemma}
	\label{lemma:winningStrategy}
	The strategy $f$ is winning.
\end{lemma}
\begin{proof}
Clearly the claim holds if the robber ever moves to \texttt{stop}, so assume
this is not the case.
Recall that at all times the  strategy maintains a loop $L$ such
that two of the cops are at $L^{\entry}$ and $L^{\exit}$.  
We do an induction on the number of loops that are nested in $L$.

So assume first that no loops are nested inside $L$.  Then $\inside(L)
=\belongs(L)$, and by Lemma~\ref{lem:move} the distance of the robber
to $L^{\exit}$ steadily decreases since $X(3)$ always moves onto the robber,
forcing it to relocate. Eventually the robber must get caught.

For the induction step, assume that there are loops nested inside
$L$.  If we ever reach step (5) in the strategy, then the enclosing loop
$L$ is changed to $L_i$, which is inside $L$ and hence has fewer loops inside
and we are done by induction.  But we must reach step (5) eventually (or
catch the robber directly), because with every execution of (3) the robber
gets closer to $L^{\exit}$:
\begin{itemize}
\item If $r\in \belongs(L)$, then this follows directly from Lemma~\ref{lem:move}
	since $X(3)$ moves onto $r$ and forces it to move.
\item If $r\in \inside(L_i)$, and we did not reach step (5), then $r$
	must have left $L_i$ using $L_i^{\exit}$.  Notice that
	$dist(r_i,L^{\exit})=dist(L_i^{\exit},L^{\exit})$ due to our
	choice of distance-function.  Also notice that
	$L_i^{\exit} \in \belongs(L)$ since $L_i$ was directly nested
	under $L$.  We can hence view the robber as having moved to
	$L_i^{\exit}$ (which keeps the distance the same) and then to
	the new position (which strictly decreases the distance by 
	Lemma~\ref{lem:move} to $L_i^{\exit}$).
\qed
\end{itemize}
%
\iffalse
			Since he cannot take a backward edge $\dist(r, L^{\exit}) > \dist(r', L^{\exit})$.
	
		\item \emph{$r \in \inside(L_i), L_i \in \mathcal{L}$}. As per step $2b$, $X(3)$ visits $L_i^{\exit}$, robber
			moves to $r'$. There are three cases:
			\begin{itemize}
				\item $r' \in \inside(L_j), L_j \in \mathcal{L}, L_j \ne L_i$. Since the robber moved from
					$L_i$ to $L_j$ and cannot take a backward edge, it follows that $\dist(L_i^{\exit}, L^{\exit}) >$ 
					$\dist(L_j^{\exit}, L^{\exit})$. Therefore, $\dist(r, L^{\exit}) > \dist(r', L^{\exit})$.
				
	\item $r' \in \belongs(L)$.  Similarly, since the robber is bound to move forward,
					$\dist(L_i^{\exit}, L^{\exit}) > \dist(r', L^{\exit})$. Hence, 
					$\dist(r, L^{\exit}) > \dist(r', L^{\exit})$.
				
	\item $r' \in \inside(L_i)$ As per step $5$ of the strategy $f$, the robber is now confined to 
		$V(L_i) = \inside(L_i)~ \cup~ \{\texttt{stop}\}$.
			\end{itemize}
	\end{itemize}
	
	It is easy to see that after at most $d_0$ moves, $\dist(r, L^{\exit})$ will be $0$. Note that since the cop $X(2)$
	occupies $L^{\exit}$, we can safely assume that after at most  $d_0$ steps, the robber will be either 
	caught or confined to the vertices $V(L_i)$ (Case~$2c$ above).
	
	Now in step $5$, we can see that the subgraph induced by the vertices $V(L_i)$, say $G_i$, has at least 
	one less loop element than $G$.
	Once $X(1)$ moves to $L_i^{\entry}$ we have the cops $X(1)$ and $X(3)$ at $L_i^{\entry}$ and $L_i^{\exit}$ respectively.
	The cops $X(2)$ and $X(3)$ can then swap roles and we are done by induction on $G_i$. 
\fi
\end{proof}

\begin{lemma}
	\label{lemma:monotoneStrategy}
	The strategy $f$ is cop-monotone.
\end{lemma}
\begin{proof}
	We must show that the cops do not re-visit a previously visited vertex at any step of the
	strategy $f$. We note that since \texttt{stop} is a sink in $G$ and the cops move to \texttt{stop}
	only if the robber was already there, it will never be visited again. Now the only steps which we need
	to verify are (2) and (5a).

Observe that while we continue in step (2), the cops $X(1)$ and $X(2)$ always stay 
at $L^{\entry}$ and $L^{\exit}$ respectively, and $X(3)$ {\em always} stays
at a vertex in $\belongs(L)$.   (This holds because $L_i$ was chosen to be
nested directly under $L$ in Case (2b), so $L_i^{\exit}\in \belongs(L)$.)   
Also notice that
$\dist(X(3), L^{\exit})=\dist(r, L^{\exit})$ for as long as we stay in step (2),
because vertices in $\inside(L_i)$ do not count towards the distance.
In the previous proof we saw that
the distance of the robber to $L^{\exit}$ strictly decreases while we
continue in step (2).  So
$\dist(X(3), L^{\exit})$ also strictly decreases while we stay
in step (2), and so $X(3)$ never re-visits a vertex.

During step (5), the cops move to $L_i^{\entry}$ and $L_i^{\exit}$ and from 
then on will only be at vertices in $\inside(L_i)\cup \{L_i^{\exit}\}$.
Previously cops were only in $\belongs(L)$ or in $\outside(L)$.   These
two sets intersect only in $L_i^{\exit}$, which is occupied throughout
the transition by $X(3)$ (later renamed to $X(2)$).  Hence no cop can
re-visit a vertex and the 
	strategy $f$ is cop-monotone.\qed
\end{proof}

With this, we have shown that the DAG-width is at most 3.  This is tight.
\begin{lemma}
	\label{lemma:counterExample}
The exists a control-flow graph that has DAG-width at least 3.
\end{lemma}
\begin{proof}
	By Theorem~\ref{lemma:dagwidthEqv}, it suffices to show that
	the robber player has a winning strategy against two cops.
	We use the graph from Fig.~\ref{fig:counterexample} and
	the following strategy:
	\begin{enumerate}
\item Start on vertex $5$.  We maintain the invariant that
	at this point the robber is at $5$ or $6$, and
	there is at most one cop on vertices $\{5,6,7,8\}$.
	This holds initially when the cops have not been placed yet.
\item If (after the next helicopter-landing) there will still be at most one
	cop in $\{5,6,7,8\}$, then move such that afterwards the robber
	is again at $5$ or $6$.  (Then return to (1).)
	The robber can always get to one of $\{5,6\}$ as follows:
	If no cop comes to where the robber is now,
	then stay stationary.  If one does, then get to the other
	position using cycle $5\rightarrow 6\rightarrow 7\rightarrow 5$;
	this cannot be blocked since one cop is moving to the robbers
	position and only one cop is in $\{5,6,7,8\}$ afterwards.
\item If (after the next helicopter-landing) both cops will be in
	$\{5,6,7,8\}$, then ``flee'' to vertex $9$ along the directed path 
	$\{5\mbox{ or }6\} \rightarrow 8 \rightarrow 1 \rightarrow 2 \rightarrow 9$.  

\item Repeat the above steps with positions $\{9,10\}$, cycle $\{9,10,11\}$ 
	and escape path $\{9\mbox{ or }10\}\rightarrow 12\rightarrow 1\rightarrow 2
	\rightarrow 5$ symmetrically.
	\end{enumerate}
	
Thus the robber can evade capture forever by toggling 
between the two loop elements $L_1$ and $L_2$ and hence has a winning strategy.\qed
\end{proof}


%From Theorem~\ref{lemma:dagwidthEqv} and Lemma~\ref{lemma:monotoneStrategy} and~\ref{lemma:winningStrategy} 
%we have the following bound on \emph{DAG-width} of control flow graphs. From Lemma~\ref{lemma:counterExample}, 
%it follows that the bound is tight.
In summary:
\begin{theorem}
	\label{theorem:cfgDagwidth}
	The DAG-width of control-flow graphs is at most $3$ and this is tight for some control-flow graphs.
\end{theorem}

\begin{figure}[t!]
	\centering
	\captionsetup[sub float]{captionskip=5pt}
	\subfloat[A control flow graph $G$]{
		\includegraphics[scale=0.35]{figures/counterExample.pdf}
		\label{fig:counterexample}
	}\hfill
	\subfloat[DAG Decomposition of G]{
		\includegraphics[scale=0.32]{figures/DAG-decompose.pdf}
		\label{fig:decomposition}
	}
	\caption{The robber player has a winning strategy on $G$ against two cops}
\end{figure}
