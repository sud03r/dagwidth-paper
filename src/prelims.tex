
%Here we review some background material and introduce notation which will
%be used later in the text.

\subsection{Control-Flow Graphs} 

\begin{definition}
	The \emph{control-flow graph} of a program $P$ is a directed graph $G=(V, E)$, where a vertex $v \in V$ represents
	a statement $s_v \in P$ and an edge $(u, v) \in E$ represents the flow of control from $s_u$ to $s_v$ under some 
	execution of the program $P$. The vertices \textnormal{\texttt{start}} and \textnormal{\texttt{stop}} correspond
	to the first and last statements of the program, respectively.
\end{definition}

For the sake of having fewer vertices in $G$, most representations of control-flow graphs combine sequence of statements
without any branching into a \emph{basic block}. This is equivalent to contracting every edge $(u, v) \in E$, such that
the \emph{in-degree} and \emph{out-degree} of $v$ is $1$. 
%(In Appendix~\ref{appendix:dagWidthProperties}, we show that this cannot increase the DAG-width.)
Throughout the paper, we assume that the control-flow graphs are derived from structured (goto-free) programs.
This is done because with unrestricted gotos any digraph could 
be a control-flow graph and so they have no special 
width properties. We can construct a control-flow graph of a structured program by parsing the 
program top-down and expanding it recursively depending on the control structures as shown in Figure~\ref{fig:controlStructures}
(see~\cite{Aho1986} for more details). 
Following the same convention as~\cite{thorup1998all}, we note that the potential successors of a statement $S$ in the program $P$ are:
\begin{itemize}
	\item \emph{out}, the succeeding statement or construct
	\item \emph{exit}, the exit point the nearest surrounding loop (\texttt{break})
	\item \emph{entry}, the entry point of the nearest surrounding loop (\texttt{continue})
	\item \emph{stop}, the end of program (\texttt{return, exit})
\end{itemize}

\begin{figure}
	\centering
	%\subfloat[A \textit{Loop Element} $L$]{
	\subfloat[\mbox{}]{\includegraphics[scale=0.54]{figures/singleLE.pdf}\label{fig:loopElement}} 
	\hfill
	%\subfloat[Sample code and resulting control-flow graph] {
	\subfloat[\mbox{}]{\includegraphics[scale=0.51]{figures/cfg-example.pdf}\label{fig:cfg-example} }
	\caption{Loop elements.  (a) The abstract structure.  
Dashed edges must start in $\belongs(L)$. (b) Sample-code and the resulting control-flow graph.
		Backward edges are dash-dotted. $\inside(L)$ is shown dotted.}
	\label{fig:controlStructures}
\end{figure}

\begin{definition} [Dominators] Let $G$ be a control-flow graph and $u, v \in V$. We say that $u$ \emph{dominates} $v$,
	if every directed path from \textnormal{\texttt{start}} to $v$ must go through $u$. Similarly, we say that
	$u$ \emph{post-dominates} $v$, if every directed path from $v$ to \textnormal{\texttt{stop}} must go through $u$.
\end{definition}

\begin{definition} [Loop Element]
	For every loop construct such as \textnormal{\texttt{do-while}}, \textnormal{\texttt{foreach}},
	we can construct an equivalent representation as a \emph{loop element} $L$, characterized
	by an entry point $L^{\entry}$ and an exit point $L^{\exit}$. See	Figure~\ref{fig:loopElement}.
\end{definition}

We note the following definitions and properties for loop elements:
\begin{itemize}
	\item We define $\inside(L)$ to be the set of vertices dominated by $L^{\entry}$
		and not dominated by $L^{\exit}$.
		Quite naturally, we define 
		$\outside(L)$ to be the set of vertices $(V \setminus \inside(L))$. 
		Note that $L^{\entry}\in \inside(L)$ 
		but $L^{\exit}\in \outside(L)$. Moreover, if we ignore edges to \texttt{stop},
		$L^{\exit}$ post-dominates vertices in $\inside(L)$.

	\item For the purpose of simplification, we assume $G$ to be enclosed in a hypothetical loop element $L_\phi$.
		This is purely notational and we do not add extra vertices or edges to $G$. We have
		$\inside(L_\phi) = V$ and $\outside(L_\phi) = \emptyset$.

	\item We say that a loop element $L_i$ is \emph{nested} under $L$, iff $L_i^{\entry} \in \inside(L)$. Two distinct loop
		elements are either nested or have disjoint insides.
	
	\item We can now associate every vertex of $G$ to a loop element as follows. We say that a vertex $v \in V$ \emph{belongs to}
		$L$ if and only if $L$ is the nearest loop element such that $L^{\entry}$ dominates $v$. More precisely, $v \in \belongs(L)$
		if and only if $v \in \inside(L)$, and there exists no $L_i$ nested under $L$ with $v \in \inside(L_i)$.
		
	\item Every $v \in V$ belongs to exactly one loop element. \texttt{start} and \texttt{stop} (as well as any vertices outside all loops of the program) belong to $L_\phi$.

	\item Finally, we say that a loop element $L_i$ is \emph{nested directly} under $L$, iff $L_i^{\exit} \in \belongs(L)$. In 
		other words, $L_i$ is nested under $L$ and there exists no $L_j$ nested under $L$ such that $L_i$ is nested under $L_j$.
\end{itemize}

We say that an edge $(u, v) \in E$ is a \emph{backward edge}, 
if $v$ dominates $u$; otherwise we call it a \emph{forward edge}. 
The following observations will be crucial:

\begin{lemma}\cite{allen1970control}
\label{lem:backward}
The backward edges are exactly those that lead from a vertex in $\belongs(L)$ to
$L^{\entry}$, for some loop element $L$.
\end{lemma}
%\begin{corollary}
% \label{lemma:FlowDAG}
%	The forward edges of a control-flow graph $G$ form an acyclic graph.
%\end{corollary}
\begin{corollary}
\label{cor:cycle}
Let $C$ be a directed cycle for which all vertices are in $\inside(L)$ and
at least one vertex is in $\belongs(L)$, for
some loop element $L$. Then $L^{\entry}\in C$.
\end{corollary}

\subsection{Treewidth and DAG-width}
The \emph{treewidth}, introduced in~\cite{SeymourRobertson1986}, is a graph theoretic concept which measures
tree-likeness of an undirected graph. We will not review the formal definition of treewidth here since we 
do not need it. Thorup~\cite{thorup1998all} showed that every control-flow graph has a treewidth of at most $6$.   This implies that any control-flow graph
has $O(n)$ edges.
%Since any graph of treewidth at most $k$ has $O(k \cdot n)$ edges, we therefore have:
%
%\begin{lemma}
%	\label{lemma:linear}
%	The number of edges in a control-flow graph is linear.
%\end{lemma}

The \emph{DAG-width}, introduced independently in~\cite{BDHK06, Obd06}, is a measure of how close a directed graph is to
being a directed acyclic graph (DAG). Like the treewidth, it is defined
via the best possible width of a so-called {\em DAG-decomposition}.
We will review the formal definition in Section~\ref{sec:decompose}.

\subsection{Cops and Robbers game}
The \emph{cops and robber} game on a graph $G$ is a two-player game in which $k$ cops attempt to catch a robber.
Most graph width measures have an equivalent characterization via a variant
of the cops and robber game.
%The game relates closely with most traditional graph width measures. 
For example, an undirected graph $G$
has \emph{treewidth} $k$ if and only if $k+1$ cops can search $G$ and successfully catch the robber~\cite{seymour1993graph}.

The DAG-width relates to the following variant of the cops and robber game
played on a directed graph $G=(V, E)$:
\begin{itemize}
	\item The cop player controls $k$ cops, which can occupy any of the $k$ vertices in the graph. We denote this set as
		$X$ where $X \in [V]^{\leq k}$. The robber player controls the robber which can occupy any vertex $r$.

	\item A play in the game is a (finite or infinite) sequence $(X_0, r_0), (X_1, r_1), \dots, (X_i, r_i)$ of positions taken
		by the cops and robbers.  $X_0=\emptyset$, i.e., the robber
		starts the game by choosing an initial position.

	\item In a transition in the play from $(X_i, r_i)$ to $(X_{i+1}, r_{i+1})$, the cop player 
%can see the robber at $r_i$ and
		moves the cops not in $(X_i \cap X_{i+1})$ to $(X_{i+1} \setminus X_i)$ with a helicopter.
		The robber can, however, see the helicopter landing and move at a great speed 
along a cop-free path to
		another vertex $r_{i+1}$.
More specifically, there must be a directed path 
		from $r_i$ to $r_{i+1}$ in the digraph $G \setminus (X_i \cap X_{i+1})$.

	\item The play is winning for the cop player, if it terminates with $(X_m, r_m)$ such that $r_m \in X_m$. If the play is infinite,
		the robber player wins.

	\item A ($k$-cop) strategy is a function $f : [V]^{\leq k} \times V \rightarrow [V]^{\leq k}$. Put differently, the cops can see the robber when
deciding where to move to.
A play is consistent
		with strategy $f$ if $X_{i+1} = f(X_i, r)$ for all $i$.
\end{itemize}

\begin{definition}(Monotone strategies)
	\label{def:monotone}
	A strategy for the cop player is called \emph{cop-monotone}, if in a play consistent with
	that strategy, the cops never visit a vertex again. More precisely, if $v \in X_i$ and $v \in X_k$
	then $v \in X_j$, for all $i \le j \le k$.
\end{definition}

The following result is central to our proof:

\begin{theorem}\textnormal{~\cite[Lemma~15 and Theorem~16]{berwanger2012dag}}
	\label{lemma:dagwidthEqv}
	A digraph G has DAG-width $k$ if and only if the cop player has a cop-monotone winning 
	strategy in the $k$-cops and robber game on G.
\end{theorem}

Therefore, in order to prove that DAG-width of a graph $G$ is at most $k$, it suffices to find a
cop-monotone winning strategy for the cop player in the $k$-cops and robber game on $G$. In the next section,
we present such a strategy and argue its correctness. We will later (in Section~\ref{sec:decompose}) give a 
second proof, not using the $k$-cops and robber game, of the DAG-width of control-flow graphs. As such, 
Section~\ref{sec:proof} is not required for our main result, but is a useful tool for gaining insight
into the structure of control-flow graphs,
and also provides a way of proving a lower bound on the DAG-width.
