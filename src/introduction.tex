Given a program $P$ and a property $\pi$, software verification concerns the problem of
determining whether or not $\pi$ is satisfied in all possible executions of $P$. The problem
can be formulated as an instance of the more general $\mu$-calculus model checking problem. Under
such a formulation, we specify the property $\pi$ as a $\mu$-calculus formula and evaluate it on
a model of the system under verification. The system is usually modeled as a state machine, like a
\emph{kripke structure}~\cite{kripke1963semantical}, where states are labeled with appropriate propositions
of the property $\pi$. For example, in the context of software systems, the \emph{control-flow graph} 
of the program is the kripke structure. The states represent the basic blocks in the program and 
the transitions represent the flow of control between them.

The complexity of the $\mu$-calculus model checking problem is still unresolved: the problem is
known to be in NP $\cap$ co-NP~\cite{EmersonJS01}, but a polynomial time algorithm has not been found. It is also known
that the problem is equivalent to deciding a winner in \emph{parity games}, a two-player game played
on a directed graph. More precisely, given a model $M$ and a formula $\phi$, we can construct a directed
graph $G$ on which the parity game is played~\cite{Stirling:2001}. Based on the winner of the game on $G$, we can determine
whether or not the formula $\phi$ satisfies the model $M$. Motivated by this development, parity games 
were extensively studied and efficient algorithms were found for special graph classes~\cite{obdrvzalek2003fast, berwanger2012dag, GanianHKLOR14}.

%^In context of 
For software model checking, the result for graphs of bounded \emph{treewidth}~\cite{obdrvzalek2003fast} 
is of particular interest since the treewidth of control-flow graphs for structured (goto-free) programs
is at most 6 and this is tight~\cite{thorup1998all}. 
The proof comes with an associated tree decomposition 
(which is otherwise hard to find~\cite{ArnborgEtAl1987}).
% by performing a simple syntactic analysis.

Obdr\v{z}\'{a}lek \cite{obdrvzalek2003fast}
gave an algorithm for parity games on graphs of treewidth
at most $k$. The algorithm runs in $O(n \cdot k \cdot d^{2(k+1)^2})$ time, where $n$ is the number of vertices and
$d$ is the number of \emph{priorities} in the game. For the $\mu$-calculus model checking problem, the number
of priorities $d$ is equal to two plus the \emph{alternation depth} of the formula, which in turn is at most $m$ (the size of the formula).
%. Since the alternation depth is at most
%$m$, the size of the formula, $d$ lies in the range $\langle2, m\rangle$.

In practice 
%it is usually the case that 
both $m$ and $d$ are usually quite small.
Since the treewidth of control-flow graphs is also small, Obdr\v{z}\'{a}lek believed that his result should give 
better algorithms for software model checking. However, as pointed out in~\cite{fearnley2011time}, the algorithm is far
from practical due to the large factor of $d^{2(k+1)^2}$. 
For example, 
%this means that 
the parity game (and hence model checking with a single sub-formula) on a control-flow graph of treewidth 6 will have a running 
time of $O(n \cdot d^{98})$. Fearnley and Schewe~\cite{fearnley2011time} have improved the run time for bounded treewidth graphs to 
$O(n \cdot (k+1)^{k+5} \cdot (d+1)^{3k+5})$. This brings the run time to $O(n \cdot 7^{11} \cdot (d+1)^{23})$, which still seems 
impractical. In the same paper, they also present an improved result for graphs of \emph{DAG-width} at most $k$, running 
in $O(n \cdot M \cdot k^{k+2} \cdot (d+1)^{3k+2})$ time. Here, $M$ is the number of edges in the DAG decomposition; no bound better than
$M \in O(n^{k+2})$ is known.

\paragraph{Contribution}
We observe that since treewidth is a measure for undirected graphs, explaining the structure of control-flow
graphs via treewidth is overly pessimistic, and by ignoring the directional properties of edges we may be losing
possibly helpful information. Moreover, software model checking could benefit from 
DAG-width based algorithms from~\cite{fearnley2011time,
DBLP:conf/csl/BojanczykDK14}, if we can find a DAG decomposition of control-flow graphs
with a small width and fewer edges.

To this end, we show that the DAG-width of control-flow graphs arising from structured (goto-free) programs 
is at most 3. Moreover, we also give a linear time algorithm to find the 
associated DAG decomposition with a linear number of edges.
%As a consequence of this result and the positive result for graphs 
%of bounded DAG-width 
Combining this with the results by Fearnley and Schewe~\cite{fearnley2011time}, parity games on control-flow graphs 
(and hence model checking with a single sub-formula) can be solved in $O(n^2 \cdot 3^{5} \cdot (d+1)^{11})$ time.
This is competitive with and probably more practical than the previous algorithms.

From a graph-theoretic perspective, it is desirable for a digraph width measure (see~\cite{GanianHKLOR14} for a brief survey)
to be small on many interesting instances. The above result makes a case for DAG-width by demonstrating that there are application
areas that benefit from efficient DAG-width based algorithms.

\paragraph{Outline} The remainder of the paper is organized as follows. Section~\ref{sec:prelims} reviews notation and 
background material, especially the \emph{cops and robbers game} and its relation to DAG-width.
The proof of the DAG-width bound follows in Section~\ref{sec:proof}. In Section~\ref{sec:decompose}, we discuss the algorithm to find
the associated DAG decomposition.
Finally, we conclude with Section~\ref{sec:conclusion}.
