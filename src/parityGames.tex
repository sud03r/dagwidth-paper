
In this section we will discuss how exactly the DAG-width based algorithm from~\cite{fearnley2011time}
for solving parity games is used for software model checking 
(which is essentially the $\mu$-calculus model checking problem on control-flow graphs).
We start with briefly discussing the modal $\mu$-calculus, parity games
and how to convert the $\mu$-calculus model checking problem 
to the problem of finding a winner in parity games. 

\subsection{Parity Game}
A \emph{parity game} $\mathcal{G}$ consists of a directed graph $G = (V, E)$ called
\emph{game graph} and a parity function $\lambda: V \rightarrow \mathbb{N}$ (called
\emph{priority}) that assigns
a natural number to every vertex of $G$.

The game $\mathcal{G}$ is played between two players $P_0$ and $P_1$ who move
a shared token along the edges of the graph $G$. The vertices $V_0 \subset V$ 
and $V_1 = V \setminus V_0$ are assumed to be owned by $P_0$ and $P_1$ respectively.
If the token is currently on a vertex in $V_i$ (for $i=0,1$), then player $P_i$ 
gets to move the token, and moves it to a successor of his choice.
This results in a possibly infinite sequence of
vertices called \emph{play}. If the play is finite, the player who is unable to move
loses the game. If the play is infinite, $P_0$ wins the game if the largest occurring
priority is even, otherwise $P_1$ wins.

A solution for the parity game $\mathcal{G}$ is a partitioning of $V$ into $V_0^{w}$ and
$V_1^{w}$, which are respectively the vertices from which $P_0$ and $P_1$ have a 
winning strategy. Clearly, $V_0^{w}$ and $V_1^{w}$ should be disjoint.

\subsection{Modal $\mu$-calculus}
The modal $\mu$-calculus (see~\cite{Stirling:2001} for a good introduction) is a fixed-point logic comprising a set of formulas defined by the
following syntax:
\begin{align*}
	\phi ::= X \mid \phi_1 \wedge \phi_2 \mid \phi_1 \vee \phi_2 \mid [\cdot]\phi \mid \langle \cdot \rangle \phi \mid \nu X .\phi \mid \mu X .\phi
\end{align*}

Here, $X$ is the set of propositional variables, and $\nu, \mu$ are maximal and minimal fixed-point operators respectively.
The \emph{alternation depth} of a formula is the number of syntactic alternations between the maximal fixed-point operator,
$\nu$, and the minimal fixed-point operator, $\mu$.

Given a formula $\phi$, we say that a $\mu$-calculus formula $\psi$ is a subformula of $\phi$, if we can obtain $\psi$ from $\phi$
by recursively decomposing as per the above syntax. For example, the formula $\nu X (P \wedge X)$ has four subformulas: $\nu X (P \wedge X)$,
$(P \wedge X)$, $P$ and $X$. The \emph{size} of a formula is the number of its subformulas.

\subsection{$\mu$-calculus Model Checking to Parity Games}

A model $M = (S, T)$ is represented as a digraph with the set of states $S$ as vertices
and the transitions $T$ as edges.  The {\em $\mu$-calculus model checking problem}
consists of testing whether  a given modal-$\mu$-calculus formula $\phi$ applies to $M$.
As mentioned earlier, given $M$ and $\phi$, there exists a way to construct a
parity game  instance $\mathcal{G} = (G, \lambda)$ such that $\phi$ applies if and
only if the parity game can be solved.
See e.g.~\cite{Stirling:2001} \footnote{Alternatively, see pages 20-23: Obdr\v{z}\'{a}lek, Jan. 
Algorithmic analysis of parity games. PhD thesis, University of Edinburgh, 2006}
We note the following relevant points of this transformation:

%\begin{Observation}~%placeholder
	\begin{enumerate} [ref={\theObservation.\arabic*}]
	\item Let $Sub(\phi)$ be the set of all subformulas of $\phi$ and $m = |Sub(\phi)|$ be the size of the formula $\phi$. 
		For every $\psi \in Sub(\phi)$ and $s \in S$ we create a vertex $(s, \psi)$ in $G$.
		Therefore, $|V(G)| = m \cdot |S|$.
	
	\item \label{obs:possibleEdges}
		For every $s \in S$, let $V_s \subseteq V(G)$ be the set of vertices of $G$ 
		$\{(s,\psi): \psi \in Sub(\phi)\}$.  Clearly, $|V_s| = m$.
		It holds that for any $s, t \in S$, there is an edge between any two vertices 
		$u \in V_s$ and $v \in V_t$ of $G$ only if $(s, t) \in T$.
		
	\item The number of priorities $d$ in $\mathcal{G}$ is equal to the alternation depth of the formula $\phi$ plus two. That is,
		for a $\mu$-calculus formula with no fixed-point operators, the number of priorities is at least $2$.
	\end{enumerate}
%\end{Observation}

\subsection{$\mu$-calculus Model Checking on Control Flow Graphs}

Recall that given a $\mu$-calculus formula of length $m$ and a control-flow graph $G = (V, E)$, we can create a 
parity game graph $G^\prime$ with $m \cdot |V|$ vertices. Now, we can use either of the treewidth or DAG-width
based algorithms from~\cite{fearnley2011time} for solving the parity game on $G^\prime$. We discuss them individually.

\paragraph{Treewidth based algorithm}
Recall that this runs in $O(|V| \cdot (k+1)^{k+5} \cdot (d+1)^{3k+5})$ time where $k$ is the treewidth of the 
game graph $G^\prime$. Using Thorup's result~\cite{thorup1998all} we can obtain a tree decomposition $(\mathcal{T}, \mathcal{X})$
of $G$ with width at most $6$. This means that each bag of the tree decomposition
contains at most $7$ vertices.
Using Observation~\ref{obs:possibleEdges}, we can now obtain a tree decomposition
$(\mathcal{T}^\prime, \mathcal{X}^\prime)$ for $G^\prime$ 
from $(\mathcal{T}, \mathcal{X})$ by replacing every $s \in \mathcal{X}_i$ by $V_s$, for all $\mathcal{X}_i \in \mathcal{X}$.
Note that the width of $\mathcal{T}^\prime$ will be $7 \cdot m-1$. 

\paragraph{DAG-width based algorithm}
This runs in $O(|V| \cdot M \cdot k^{k+2} \cdot (d+1)^{3k+2})$ time where $k$ is the DAG-width of the
game graph $G^\prime$ and $M$ is the number of edges in the DAG decomposition. Using our main result 
(Theorem~\ref{theorem:result}), we can obtain a DAG decomposition $(D, X)$ of width $3$ and $M \in O(|V|)$.
As in the previous case, we can obtain a DAG decomposition of $G^\prime$ from $(D,X)$ by replacing every 
$s \in X_i$ with $V_s$, for all $X_i \in X$. Note that this will have width $3 \cdot m$ and $M \in O(|V|)$.


\medskip
We can see that even for the smallest possible values $m = 1$ and $d = 2$, the treewidth based algorithm runs in
$O(|V| \cdot 7^{11} \cdot 3^{23}) = O(|V| \cdot 10^{20})$ time. For the same values,
the DAG-width based algorithm runs in $O(|V|^2 \cdot 3^{5} \cdot 3^{11}) = O(|V|^2 \cdot 10^{7})$,
which is better unless $|V|\geq 10^{13}$.  Of course the actual run-times may 
be influenced  by the constants hidden behind the asymptotic notations, but it
is fair to assume that the DAG-width based algorithm will be faster for most
practical scenarios, especially as $m$ and $d$ increase.

\iffalse
% moved to conclusion
\paragraph{}
However, we note that for bigger formulas, even the DAG-width based algorithm is not very practical
as the exponent of $(d+1)$ increases rapidly. We conclude our discussion with two possible directions
for further research. First, it is quite likely that a bound for DAG-width of the game graph $G^\prime$
better than $3 \cdot m$ exists. A smaller bound can result in a more practical algorithm for software model checking. 
Second, it may be worthwhile to solve the parity game for the special case of control-flow graphs using the
DAG decomposition computed by our algorithm from Section~\ref{sec:decompose}.
\fi

