
We already showed that control-flow graphs have DAG-width at most 3 (Theorem~\ref{theorem:cfgDagwidth}). In this section we
show how we can construct an associated DAG decomposition with few edges.
\subsection{DAG-width}
We first state precise definition of DAG-width, for which we need the following notation.
%A \emph{directed acyclic graph} (DAG) is a graph that contains no directed cycles. 
For a directed acyclic graph (DAG) $D$, 
use $u \preceq_D v$ to denote that there is a directed path 
from $u$ to $v$ in $D$.
%The DAG-width of a digraph is then defined via a graph decomposition as follows:
\begin{definition} [DAG Decomposition] Let $G = (V, E)$ be a directed graph. A DAG decomposition of $G$ consists of a DAG $D$
	and an assignment of bags $X_i \subseteq V$ to every node $i$ of $D$ such that:
	\begin{enumerate}
		\item \emph{(Vertices covered)} $\bigcup X_i = V$.
		
		\item \emph{(Connectivity)} 
For any $i\preceq_D k \preceq_D j$ we have
			$X_i \cap X_j \subseteq  X_k$.
		
\item \emph{(Edges covered)} 
\begin{enumerate}
\item For any source $j$ in $D$, any $u \in X_j$, and any edge $(u, v)$ 
in $G$, there exists a successor bag $X_k$ of $X_j$ with $v \in X_k$.
\item For every arc $(i, j)$ in $D$, any $u \in (X_j \setminus X_i)$, 
and any edge $(u, v)$ in $G$, there exists a successor-bag $X_k$ of $X_j$
with $v \in X_k$.
\end{enumerate}
Here a \emph{successor-bag} of $X_j$ is a bag $X_k$ with $j \preceq_D k$.
	\end{enumerate}
\end{definition}

Note that for the ease of understanding we have rephrased the edge-covering condition of 
the original in~\cite{berwanger2012dag}. 
(Appendix~\ref{appendix:eqv} proves the equivalence.)

\subsection{Constructing a DAG decomposition}
While we already know that the DAG-width of control-flow graphs is at most
3, we do not know the DAG-decomposition and its number of edges $M$ that
is needed for the runtime for~\cite{fearnley2011time}.
There is a method to get the DAG decomposition from a winning strategy for the
$k$-cops and robber game~\cite{berwanger2012dag},
but it only shows $M \in O(n^{k+2})$. 
We now construct a DAG decomposition for control-flow graphs 
directly. Most importantly, it has $M \in O(n)$ edges,
thereby making~\cite{fearnley2011time} even more efficient for control-flow graphs.
%Moreover, it can be found in linear time.
%and can be easily modified to a DAG decomposition that is `nice' (in the 
%sense of~\cite{berwanger2012dag}).

Let $G = (V, E)$ be a control-flow graph. We present the following algorithm to construct a DAG decomposition $(D, X)$ of 
$G$.

\begin{Algorithm}[Construct the DAG]
	\label{algo:constructDAG}
	\begin{enumerate}
	\item Start with $D = G$. That is $V(D) = V$ and $E(D) = E$.
	
	\item  Remove all backward arcs.  Thus, 
	let $E_e$ be all backward edges of $G$;  
	recall that each of them connects from a node $v\in \belongs(L)$ 
		to $L^{\entry}$, for some loop element $L$.
	Remove all arcs corresponding to edges in $E_e$ from $D$.
		
	\item Remove all arcs leading to a loop-exit.  Thus, let $E_x$ be
	all edges $(u, v)$ in $G$ such that 
	$u \in \belongs(L)$ and $v = L^{\exit}$ for some loop element $L$.
	Recall that these arcs are attributed to \texttt{break} statements.
	Remove all arcs corresponding to edges in $E_x$ from $D$.
	
	\item Re-route all arcs leading to a loop-entry.  Thus, let $E_m$ be 
	all edges $(u, v)$ in $G$ such that 
	$u \in \outside(L) \setminus L^{\exit}$ and $v = L^{\entry}$ for some loop 
	element $L$.    For each such edge, remove the corresponding 
	edge in $D$ and replace it by an arc $(u,L^{\exit}$).  Let $A_m$ be
	those re-routed arcs.
	Note that now $\indeg_D(L^{\entry}) = 0$ since we also 
	removed all backward edges.

	\item Reverse all arcs surrounding a loop.  Thus, let
	$E_r$ be the edges in $G$ of the form $(L^{\entry}, L^{\exit})$ for some
	loop element $L$.  For each edge, reverse the corresponding
	arc in $D$.  Let $A_r$ be the resulting arcs.
	\end{enumerate}
\end{Algorithm}

%We will soon show that $D$ constructed as above is actually a DAG. 
Note that there is a bijective mapping $\mathcal{D}$ from the vertices 
in $G$ to the nodes in $D$.
For ease of representation, assume that $v_1, v_2, .., v_n$ are
the vertices of $G$ and $1, 2, .., n$ are the corresponding nodes in $D$. 
%That is, $\mathcal{D}(v_i) = i$. Likewise, $\mathcal{D}^{-1}(i) = v_i$.
%
We now fill the bags $X_i$:
% of the DAG decomposition $(D, X)$ as follows: 

%\begin{Algorithm}[Fill bags of the DAG decomposition]
%	\label{algo:fillBags}
%	\begin{enumerate}
%		\item[] 
\begin{quotation}
For every  vertex $v_i \in V$, set $X_i \coloneqq \{v_i, L^{\entry}, L^{\exit}\}$,
				where $L$ is the loop element such that $v_i \in \belongs(L)$.
\end{quotation}
%	\end{enumerate}
%\end{Algorithm}

A sample decomposition is shown in Figure~\ref{fig:decomposition}.  Clearly the construction can be done in linear time and
digraph $D$ has $O(|E|)=O(n)$ edges.  One easily verifies the following:

\begin{observation}
	\label{obs:uniqueIntroduce}
For every arc $(i, j) \in E(D)$, $X_j \setminus X_i = v_j$.
%We say that vertex $v_j$ is {\em introduced} in bag $j$.
\end{observation}

It remains to show that $(D, X)$ is a valid DAG decomposition.
%for the control-flow graph $G$. 
%To this end, we make following observations.
\begin{enumerate}
	\item \textbf{$D$ is a DAG}. 
We claim that $G-E_e$ is acyclic.  For if it contained a directed cycle $C$,
then let $L$ be a loop element with $C\subseteq \inside(L)$, but $C\not\subseteq 
\inside(L_i)$ for any loop element $L_i$ nested under $L$.  Therefore $C$
contains a vertex of $\belongs(L)$. By Corollary~\ref{cor:cycle} then $L^{\entry}$
belongs to $C$, so $C$ contains a backward edge.  This is impossible since we
delete the backward edges.

Adding arcs $A_m$ cannot create a cycle since each arc in it is a shortcut
for the 2-edge path from outside $L$ to $L^{\entry}$ to $L^{\exit}$.
In $G-E_e-E_x$ there is no directed path from $L^{\entry}$ to $L^{\exit}$, since
such a path would reside inside $L$, and the last edge of it belongs to $E_x$.
In consequence adding arcs $A_r$ cannot create a cycle either.  Hence $D$ is acyclic.
\item \textbf{Vertices Covered}.  By definition each $v_i$ is contained in its bag $X_i$.
\item \textbf{Connectivity}.  Let $i\preceq_D k \preceq_D j$ be three nodes
	in $D$.  Recall that their three bags are
	$\{v_i,L_i^{\entry},L_i^{exit}\}$,
	$\{v_k,L_k^{\entry},L_k^{exit}\}$ and
	$\{v_j,L_j^{\entry},L_j^{exit}\}$, where $L_i,L_j,L_k$ are the
	loop elements to which $v_i,v_j,v_k$ belong.
	Nothing is to show unless $X_i\cap X_j\neq \emptyset$, which 
	severely restricts the possibilities:
\begin{enumerate}
\item 
Assume first that $L_i = L_j=L$.  Thus $v_i$ and $v_j$ belong to the
same loop element, and by the directed path between them, so does $k$.
So the claim holds since $X_i\cap X_j = \{L^{\entry},L^{\exit}\}$.

\item If $L_i \ne L_j$, then the intersection can be non-empty only if
$v_i = L_j^{\exit}$ (recall that $i\preceq_D j$).  But then
$X_i\cap X_j=\{L_j^{\exit}\}$, and the path from $i$ to $j$ must go
from $\mathcal{D}(L_j^{exit})$ to $\mathcal{D}(L_j^{\entry})$ to $j$.  
It follows that $v_k$ also belongs to $L_j$ and so $L_j^{\exit} \in X_k$
and the condition holds.
\end{enumerate}

	\item \textbf{Edges Covered}.
We only show the second condition; the first one is similar
	and easier since \texttt{start} is the only source.  Let $(i,j)$ be an arc in $D$.
By Observation~\ref{obs:uniqueIntroduce}, 
		$v_j$ is the only possible vertex in $X_j \setminus X_i$.
		Let $e = (v_j, v_l)$ be an edge of $G$. We have the following cases:
		\begin{enumerate}
			\item If $e \in (E_e \cup E_x \cup E_r)$, then $v_l \in \{L^{\entry}, L^{\exit}\}$ and $X_j = \{v_j, L^{\entry}, L^{\exit}\}$
				itself can serve as the required successor-bag.
			\item If $e \in E_m$, then $v_l = L^{\entry}, v_j \in \outside(L)$. 
				We re-routed $(v_j,L^{\entry})$ as arc $(j,\mathcal{D}(L^{\exit})$
				and later added an arc $(\mathcal{D}(L^{\exit}),\mathcal{D}(L^{\entry}))$,
				so $l$ is a successor of $j$ and $X_l$ can serve  as the
				required successor-bag.
			\item Finally, if $e \in E \setminus (E_e \cup E_x \cup E_r \cup E_m)$, 
				then $(j, l)$ is an arc in $D$ and $X_l$ is the required successor-bag.
		\end{enumerate}
\end{enumerate}


%In Appendix~\ref{appendix:niceDAG}, we 
%show how to transform this DAG decomposition into a so-called `nice' one
%without increasing the DAG-width and maintaining asymptotically the same size. 
We conclude:

\begin{theorem}
	\label{theorem:result}
	Every control-flow graph $G = (V, E)$ has a DAG decomposition of 
width 3 with $O(|V|)$ vertices and edges.
	It can be found in linear time.
\end{theorem}

\comment{TB: I've thrown out all mentioning of ``nice''.  The proof you give
uses nothing but the face that we have $O(n)$ edges in the DAG-decomposition,
and hence is rather trivial.}

%\paragraph{Runtime} It is not hard to see that the algorithm runs in $O(|E|)$ time since we process every edge of $G$ at most once.
%From Lemma~\ref{lemma:linear}, it follows that $|E| = O(|V|)$ and hence the algorithm runs in $O(|V|)$
%time. Moreover, we create at most one edge in the DAG decomposition for every edge in $G$. Therefore, the number of 
%edges in the computed DAG decomposition is also linear in the size of the graph.
