TEXSRC := paper.tex
AUX := $(TEXSRC:.tex=.aux)
PDF := $(TEXSRC:.tex=.pdf)

all:
	pdflatex $(TEXSRC)
	bibtex $(AUX)
	pdflatex $(TEXSRC)
	pdflatex $(TEXSRC)

clean:
	\rm -f *.aux *.log *.bbl *.blg *.pdf 


